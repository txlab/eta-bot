###########
# BUILDER #
###########
FROM node:18-alpine3.15 as builder
# >15 to fix: https://github.com/nodejs/node/issues/35824#issuecomment-732954955
# alpine != 3.13/.14? to fix: https://github.com/tsightler/ring-mqtt/issues/237#issuecomment-903309986

WORKDIR /app

# Copy package.json and yarn.lock before copy other files for better build caching
COPY ["package.json", "yarn.lock", "tsconfig.json", "./"]
RUN yarn --network-timeout=300000 --frozen-lockfile

# Add source files
COPY ["src/", "src/"]
RUN yarn build

#############
# PROD_DEPS #
#############
FROM node:18-alpine3.15 as prod_deps

WORKDIR /app

# Copy package.json and yarn.lock before copy other files for better build caching
COPY ["package.json", "yarn.lock", "tsconfig.json", "./"]
RUN yarn --network-timeout=300000 --frozen-lockfile --production


##########
# RUNNER #
##########
FROM node:18-alpine3.15 as runner

RUN mkdir -p /app/data && chown -R 1000 /app
WORKDIR /app
VOLUME [ "/app/data" ]

# Copy dependencies from prod_deps stage
COPY --from=prod_deps /app/package.json /app/
COPY --from=prod_deps /app/node_modules/ /app/node_modules/
# Copy app files from builder stage
COPY --from=builder /app/dist /app/dist

USER 1000
CMD ["node", "dist/app.js"]
