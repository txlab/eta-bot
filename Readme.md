# Telegram-Bot zur Steuerung von ETA-Heizungen


Telegram Bot zur Koordination & Benachrichtigung bei benoetigtem Heizen, zur Koordinierung wer es macht, und zur Einsicht wie viel eine Ladung Holz den Pufferspeicher geladen hat.
Bei Bedarf kann auch automatisch auf den Pellet-Modus umgeschaltet werden.

![Screenshot Heizvorgang](https://gitlab.com/txlab/eta-mate/-/wikis/uploads/10c9af6ba5f2a70cb1bcaa83b4be9d7a/Screenshot_from_2021-10-19_14-05-23.png)
![Screenshot Bereitschaft](https://gitlab.com/txlab/eta-mate/-/wikis/uploads/4dbd5b029eba64358f0052cf31d6c747/Screenshot_20211019-114523.jpg)

## Dev Setup
1. `cp .env.example .env` & adapt
2. `yarn`
3. `yarn dev` (or `yarn build-start` which doesn't auto-restart)

## Docker deploy
1. Copy `docker-compose.yml` and `.env.example` to some directory of your choice
2. `cp .env.example .env` & adapt
3. Edit `docker-compose.yml` if needed
4. `docker-compose up -d`


### Related project: [InfluxDB fetcher](https://gitlab.com/TeNNoX/eta-influx)

Bei Fragen kontaktiert mich gern, bin noch nicht dazu gekommen gross zu dokumentieren.