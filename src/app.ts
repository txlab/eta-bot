import _ from 'lodash'
import TeleBot from 'telebot'
import logger from './util/logger.js'
import { TELEGRAM_ADMIN_CHAT, TELEGRAM_CHAT, TELEGRAM_TOKEN } from './util/secrets.js'
import { StatusPoster } from './modules/status-poster.js'
import { TeleBotExtended } from 'telebot-extension'
import { api, ETA } from './modules/eta-api.js'
import * as Sentry from '@sentry/node'
import { db, initDB } from './data/db.js'
import { extendTeleBot } from './util/telebot-extensions.js'
import { DateTime } from 'luxon'

const INTERVAL = 10000

Sentry.init()

// TODO: refactor to telegraf bc. of https://github.com/mullwar/telebot/issues/148
const bot: TeleBotExtended = extendTeleBot(new TeleBot({
  token: TELEGRAM_TOKEN, // Required. Telegram Bot API token.
  polling: { // Optional. Use polling.
    interval: 500, // Optional. How often check updates (in ms).
    timeout: 10000, // Optional. Update polling timeout (0 - short polling).
    limit: 10, // Optional. Limits the number of updates to be retrieved.
    retryTimeout: 30000 // Optional. Reconnecting timeout (in ms).
  }
}))

bot.on('tick', (msg: TGMessage) => {
  logger.debug('[bot.tick]')
  // msg.reply.text(msg.text)
})

bot.on('text', (msg: TGMessage) => {
  logger.info('Received message: %o', msg)
  // msg.reply.text(msg.text)
})

// MAIN //
;(async () => {
  logger.info('Starting app')
  await initDB()
  logger.debug('DB: %o', db.data)

  const statusPoster = new StatusPoster(bot, TELEGRAM_CHAT /* TELEGRAM_ADMIN_CHAT */)

  bot.on('callbackQuery', async (event: TGCallbackQuery) => {
    logger.info('callbackQuery: %O', event)
    try {
      if (event.data === 'signup') {
        await statusPoster.handleSignup(event)
      } else if (event.data.startsWith('pellet-')) {
        await statusPoster.handlePellet(event)
      } else logger.error('Unknown callbackQuery data: %s', event.data)
    } catch (e) { logger.error('callbackQuery handler', e) }
  })

  // Commands //
  let overrides: { status?: string } = {}
  bot.addCmd(/^\/status (.+)$/, (msg, [full, status]) => {
    if (msg.from.id !== _.parseInt(TELEGRAM_ADMIN_CHAT)) { return msg.reply.text("You're not an admin") }
    overrides.status = status
    logger.info('Overrides: %o', overrides)
    return msg.reply.text(`Status set to: '${status}'`)
  })
  bot.addCmd(/^\/pellet (.+)$/, async (msg, [full, arg]) => {
    if (msg.from.id !== _.parseInt(TELEGRAM_ADMIN_CHAT)) { return msg.reply.text("You're not an admin") }
    if (!['on', 'off'].includes(arg)) { return msg.reply.text(`Invalid arg: ${arg}`) }
    await api.setPellet(arg === 'on')
    return msg.reply.text(`Pellet set to: '${arg}'`)
  })
  let killswitch = false
  bot.addCmd(/^\/killswitch (.+)$/, (msg, [full, arg]) => {
    if (msg.from.id !== _.parseInt(TELEGRAM_ADMIN_CHAT)) { return msg.reply.text("You're not an admin") }
    if (!['engage', 'disengage'].includes(arg)) { return msg.reply.text(`Invalid arg: ${arg}`) }
    killswitch = arg === 'engage'
    return msg.reply.text(`KillSwitch set to: '${_.toString(killswitch)}'`)
  })

  // Start Telegram update fetching
  bot.start()

  // Main Loop //
  void (async () => {
    let eta: ETA | null = null
    try {
      while (true) {
        if (!killswitch) {
          try {
            eta = await api.query()
          } catch (err) {
            if (err.message?.includes('EHOSTUNREACH')) {
              logger.warn('Failed to query ETA: %s', err.message)
            }
            logger.error(err, 'ETA API error: %s', err.message)
          }
          if (eta && !eta.status) {
            // handle parsing failures gracefully (API responses are sometimes flaky)
            logger.warn('eta.status is null, discarding all data: %o', eta)
            eta = null
          }
          _.assign(eta, overrides)
          overrides = {}
          logger.debug('ETA state: %s%% "%s" %s', eta && _.round(eta?.pufferPercent), eta?.status, eta?.statusExtra)

          if (eta) Sentry.setContext('eta', eta)
          await statusPoster.update(eta) // unset at the end of this block

          // Save timestamp to check if data is stale when reading it
          _.set(db, 'data.time', DateTime.local())
          await db.write()

          Sentry.setContext('eta', null)
        }

        // sleep
        await new Promise(resolve => setTimeout(resolve, INTERVAL))
      }
    } catch (err) {
      logger.error('MainLoop Error: %s\n%s', err.message ?? JSON.stringify(err), err.stack)
      Sentry.captureException(err, { extra: { db: db.data, eta } })

      try {
        await bot.adminMessage(`Mainloop error:\n${err.message ?? JSON.stringify(err, undefined, 4)}`)
      } catch (err) {
        logger.error('Failed to send admin message for error because: %s', err)
        Sentry.captureException(err)
      }

      _.defer(() => process.exit(1))
    }
  })()
})().catch((err: Error) => {
  console.error('Main function Error:', err)
  logger.error('Main function Error: %o', err)
  Sentry.captureException(err)
  _.defer(() => process.exit(1))
})
