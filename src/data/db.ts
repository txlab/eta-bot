import logger from '../util/logger.js'
import { Low } from 'lowdb'
// @ts-expect-error
import { JSONFile } from 'lowdb/node'
import { StatusPosterData, StatusPosterDataInitial, deserializeStatusPosterData } from './status-poster-data.js'
import { DateTime } from 'luxon'
import _ from 'lodash'

export interface Data {
  /** timestamp to check if data is stale when reading it */
  time: DateTime | null
  status: StatusPosterData
}

export const db = new Low<Data>(new JSONFile<Data>('data/data.json'))

export async function initDB () {
  await db.read()
  logger.debug('DB raw read: %o', db.data)
  if (db.data == null) {
    db.data = { time: null, status: StatusPosterDataInitial }
  }

  // Deserialize //
  if (db.data?.time && _.isString(db.data?.time)) db.data.time = DateTime.fromISO(db.data.time)
  db.data.status = deserializeStatusPosterData(db.data.status, db.data.time)
}
