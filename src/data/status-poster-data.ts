import { DateTime } from 'luxon'
import logger from '../util/logger.js'

const STALE_DATA_THRESHOLD_HOURS = 6 // e.g. when the bot was only restarted but the heating was in progress from 3 hours ago

export interface StatusPosterData {
  msgID: number | null
  signedUp: TGUser | null
  reportMsg: {
    id: number
    pufferBegin: number
    pufferEnd?: number
    start: DateTime
    end?: DateTime
    /** who signed up when the heating started */
    signedUp: TGUser | null
  } | null
}

export const StatusPosterDataInitial: StatusPosterData = { msgID: null, signedUp: null, reportMsg: null }

export const deserializeStatusPosterData = (data: any, time: DateTime | null) => {
  if (data?.reportMsg?.start) data.reportMsg.start = DateTime.fromISO(data.reportMsg.start)
  if (data?.reportMsg?.end) data.reportMsg.end = DateTime.fromISO(data.reportMsg.end)

  // Cleanup potentially stale DB
  if (time === null) {
    logger.info('Discarding stale reportMsg (no time given)')
    return StatusPosterDataInitial
  }
  const ageHours = -time.diffNow('hours').hours
  logger.info('Checking if status data is stale (%d hours old): %o', ageHours, data)
  if (ageHours > STALE_DATA_THRESHOLD_HOURS) {
    logger.info('Discarding stale reportMsg (%d hours old)', ageHours)
    return StatusPosterDataInitial
  }

  return data as StatusPosterData
}
