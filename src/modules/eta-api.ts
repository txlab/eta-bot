import _ from 'lodash'
import { DateTime } from 'luxon'
import fs from 'fs'
import axios from 'axios'
// @ts-expect-error
import XmlReader from 'xml-reader'
import logger from '../util/logger.js'
import { ETA_URL } from '../util/secrets.js'

export interface ETA {
  time: DateTime
  pufferPercent: number
  pufferTemp: number
  hausVorlauf: number
  status: string
  statusExtra: string
  pelletOn: boolean
  pelletStatus: string
  pelletStatusExtra: string
}

export default class EtaAPI {
  async query (): Promise<ETA> {
    const time = DateTime.local()
    const response = await axios.get(ETA_URL + '/user/vars/etalyze', { responseType: 'text' })
    // logger.debug('XML\n%s', response.data)
    fs.writeFileSync('last-result.xml', _.toString(response.data)) /*, ()=>{} ) */

    const xml = XmlReader.parseSync(response.data)

    let someFailed = false
    const find = (uri: string, type: 'string' | 'valueDecimal' | 'strValInt' = 'string') => {
      const result = _.find(xml.children[0].children, ['attributes.uri', uri])
      if (result === undefined) {
        logger.error("Failed to find ETA uri '%s'", uri)
        someFailed = true
        return null
      }
      if (type === 'valueDecimal') return Number(parseInt(result.children[0].value) / 10)
      if (type === 'strValInt') return parseInt(result.attributes.strValue)
      else return result.attributes.strValue
    }

    const pufferPercent = find('120/10601/0/0/12528', 'valueDecimal')
    const pufferTemp = find('120/10601/0/11327/0')
    const hausVorlauf = find('120/10101/0/11060/0')
    // const hausVorlaufVar = find('120/10101/0/11060/0')
    // const hausVorlauf = Number(parseInt(hausVorlaufVar.children[0].value) / 10)
    const status = find('48/10391/0/0/19402')
    const statusExtra = find('48/10391/0/0/19391')

    const pelletOn = find('40/10401/0/0/12080') === 'Ein'
    const pelletStatus = find('40/10401/0/0/19402')
    const pelletStatusExtra = find('40/10401/0/0/19391')

    if (someFailed) {
      logger.error("Some variables failed to be found, here's the original response: " + _.toString(response.data))
    }

    return { time, pufferPercent, pufferTemp, hausVorlauf, status, statusExtra, pelletOn, pelletStatus, pelletStatusExtra }
  }

  async setPellet (status: boolean) {
    logger.info('Setting pellet to: %s', status)
    try {
      const result = await axios.post(ETA_URL + '/user/var/40/10401/0/0/12080/', `value=${status ? 1803 : 1802}`) // straigh-forward intuitive api, huh?
      logger.debug('pellet result: %o', result.data)
    } catch (err) {
      logger.error('Pellet command failed: %s\n%o', err, err.response.data)
      throw err
    }
  }
}

export const api = new EtaAPI()
