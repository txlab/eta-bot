import _ from 'lodash'
import logger from '../util/logger.js'
import { DateTime } from 'luxon'
import { TeleBotExtended } from 'telebot-extension'
import { api, ETA } from './eta-api.js'
import emoji from 'node-emoji'
import { db } from '../data/db.js'
import { StatusPosterData } from '../data/status-poster-data.js'
import { CopyWithPartial } from 'utils.js'

const SIGNUP_THRESHOLD = 50
const WARN_THRESHOLD = 30
const CRITICAL_THRESHOLD = 15
const MIN_VORLAUF = 40 // degree celcius below which a warning is issued that it might get cold
const STATUSES = new Map([
  ['Bereit', 'idle'],
  ['Heizen', 'heating'],
  ['Glutabbrand', 'heating'],
  ['Startvorgang', 'IGNORE'], // doesn't mean it's going to heat, only that the door was opened
  ['kein Stückholz eingelegt', 'IGNORE'], // means: Pellet off, no wood... so, no extra info
  ['TWIN Betrieb:', 'pellet'],
  ['Pelletsbetrieb:', 'pellet'],
])
const STATUS_DISPLAY = new Map([ // to override status names
  ['Heizen', 'Heizvorgang'], // easily confused with the imperative :P (true story)
  ['TWIN Betrieb:', 'PelletAutomatik'],
])

export class StatusPoster {
  bot: TeleBotExtended
  chatID: string
  msgHasKeyboard = false
  last: ETA | null = null
  warned = false

  constructor (bot: TeleBotExtended, chatID: string) {
    this.bot = bot
    this.chatID = chatID
    if (db.data == null) throw new Error('db.data is null')
  }

  get data (): StatusPosterData {
    if (db.data == null) throw new Error('db.data is null')
    return db.data.status
  }

  async update (eta: ETA | null) {
    const timeStr: string = (eta?.time ?? DateTime.local()).toLocaleString(DateTime.TIME_24_WITH_SECONDS)
    let statusMeaning: string | undefined
    if (eta != null) {
      statusMeaning = STATUSES.get(eta.status)
      if (statusMeaning === undefined) {
        await this.bot.adminMessage('unknown status: ' + eta.status)
        STATUSES.set(eta.status, 'IGNORE') // so I don't get consecutive messages
        statusMeaning = 'IGNORE'
      }
    } else {
      statusMeaning = 'IGNORE'
    }

    // Log changes
    if (eta && this.last) {
      if (eta.pelletOn !== this.last.pelletOn) { logger.info('Pellet state changed to: ' + (eta.pelletOn ? 'ON' : 'OFF')) }
    }

    // eta.pufferPercent = this.pufferPercent
    // if (this.signedUp != null && eta !== this.last) {
    //   eta.status = 'Heizen'
    // } else { this.pufferPercent -= 2 }
    // eta.status += eta.time.toLocaleString(DateTime.TIME_24_SIMPLE)

    // REPORT //
    if (this.data.msgID !== null) {
      // reset values if heating
      if (eta != null && statusMeaning === 'heating') {
        this.warned = false

        if (this.data.reportMsg === null || this.data.reportMsg.end) {
          logger.info('Replacing with report message [#%s]', this.data.msgID)
          try {
            await this.bot.deleteMessage(this.chatID, this.data.msgID)
          } catch(err) {
            logger.error("Failed to delete old reportMsg:", err ,{msgID:this.data.msgID})
          }
          const reportMsg = { pufferBegin: eta.pufferPercent, start: eta.time, signedUp: this.data.signedUp }
          const msg = await this.bot.sendMessage(
            this.chatID, this.formatReportMsg(reportMsg),
            { parseMode: 'HTML', replyMarkup: null, notification: false }
          )
          logger.info('Sent reportMsg [#%s]', msg.message_id)
          this.data.reportMsg = { ...reportMsg, id: msg.message_id }
          this.data.msgID = null // force new msg (no notification as warned=false)}}
          this.msgHasKeyboard = false
        }

        this.data.signedUp = null
        await db.write()
      }

      // Complete/Update Report
      if (eta != null && this.data.reportMsg !== null && !['heating', 'IGNORE'].includes(statusMeaning)) {
        if (_.round(eta.pufferPercent) > _.round(_.defaultTo(this.data.reportMsg?.pufferEnd, 0))) { // Either not finalized yet or Puffer got a bit hotter still
          logger.info(`${this.data.reportMsg?.pufferEnd ? 'Updating' : 'Saving'} report message, as: %s > %s`, eta.pufferPercent, this.data.reportMsg?.pufferEnd)
          this.data.reportMsg.pufferEnd = eta.pufferPercent
          this.data.reportMsg.end = eta.time
          try {
            await this.bot.safeEditMessage(
              { chatId: this.chatID, messageId: this.data.reportMsg.id },
              this.formatReportMsg(),
              { parseMode: 'HTML', replyMarkup: null } // TODO: thanks button?
            )
          } catch (err) {
            if (err.description?.includes('message to edit not found')) {
              logger.error('Error: %o - resetting reportMsg', err)
              this.data.reportMsg = null
            } else throw err
          }
          await db.write()
        }
      }
    }

    // Send/Update Status message
    if (this.data.msgID == null || this.shouldSendNew(eta, statusMeaning)) {
      logger.info('Sending new msg')
      if (this.data.msgID != null) {
        await this.bot.deleteMessage(this.chatID, this.data.msgID) // In case we need to send a new message to warn, we don't need the old
        // if (this.msgHasKeyboard) await this.bot.editMessageReplyMarkup({ chatId: this.chatID, messageId: this.msgID }, {});
        // await this.bot.safeEditMessage( { chatId: this.chatID, messageId: this.msgID }, "_deleted_", { parseMode: 'MarkdownV2', replyMarkup:  }, )
      }
      const text = this.formatMsg(eta, statusMeaning)
      logger.debug('Message text: %s', text)
      const msg = await this.bot.sendMessage(
        this.chatID, text,
        { parseMode: 'HTML', replyMarkup: this.makeKeyboard(eta, statusMeaning), notification: this.warned }
      )
      this.data.msgID = msg.message_id
      await db.write()
    } else {
      // update last message
      logger.debug('Editing msg %s', this.data.msgID)
      try {
        await this.bot.safeEditMessage(
          { chatId: this.chatID, messageId: this.data.msgID },
          this.formatMsg(eta, statusMeaning),
          { parseMode: 'HTML', replyMarkup: this.makeKeyboard(eta, statusMeaning) }
        )
      } catch (err) {
        if (err.description?.includes('message to edit not found')) {
          logger.error('Error: %o - resetting msgID', err)
          this.data.msgID = null
          await db.write()
        } else throw err
      }
    }

    this.last = eta
  }

  shouldSendNew (eta: ETA | null, statusMeaning: string): boolean {
    if (this.data.msgID === null) {
      this.warned = true // otherwise, if we start out in warning state, there will be two messages sent (as in the next loop, we would think we didn't warn yet)
      return true
    }
    if (eta === null) {
      if (!this.last === null) {
        logger.debug('ETA offline, last is not null - warning')
        return true
      } else {
        logger.debug('ETA offline, last is null - not warning')
        return false
      }
    }
    // if (eta.status !== this.last.status && statusMeaning !== 'IGNORE') {
    //   logger.debug('New status: ' + statusMeaning)
    //   return true
    // }
    if (
      (this.data.signedUp === null || eta.pufferPercent <= CRITICAL_THRESHOLD)
      && !this.warned
      && eta.pufferPercent <= WARN_THRESHOLD
      && statusMeaning !== 'heating'
      && statusMeaning !== 'IGNORE'
    ) {
      logger.debug('Need to warn')
      this.warned = true
      return true
    }

    return false
  }

  formatReportMsg (reportMsg: CopyWithPartial<NonNullable<StatusPosterData['reportMsg']>, 'id'> | null = this.data.reportMsg) {
    if (reportMsg == null) return 'error: no reportMsg'
    logger.debug('formatReportMsg data: %o', reportMsg)

    let msg = `Geheizt bei:  <b>${_.round(reportMsg.pufferBegin).toString()}%</b> um <b>${reportMsg?.start?.toLocaleString(DateTime.TIME_24_SIMPLE)}</b>`
    if (reportMsg.pufferEnd != null) {
      msg += `\n${emoji.get('white_check_mark')} Endwert: <b>${_.round(reportMsg.pufferEnd).toString()}%</b>`
        + (reportMsg.end ? ` um <b>${reportMsg.end?.toLocaleString(DateTime.TIME_24_SIMPLE)}</b>` : '')
    }
    if (reportMsg.signedUp?.first_name) {
      msg += `\n<i>\\(${reportMsg.signedUp?.first_name.split(' ')[0]}\\)</i>`
    }
    return msg
  }

  formatMsg (eta: ETA | null, statusMeaning: String) {
    let msg = ''

    if (eta === null) {
      return '<b>Fehler: Heizung nicht erreichbar</b>\n'
    }

    if (this.data.signedUp != null) {
      msg += `<b>${this.data.signedUp.first_name.split(' ')[0]} macht's</b>\n`
    } else if (eta.pufferPercent <= WARN_THRESHOLD && statusMeaning !== 'heating') {
      msg += '<b>Heizen benötigt</b>\n'
    }
    if (statusMeaning === 'heating') msg += `${emoji.get('fire')} `

    msg += `<u>${STATUS_DISPLAY.get(eta.status) ?? eta.status} `
    // if (_.round(this.last.pufferPercent) !== _.round(eta.pufferPercent)) {
    // msg += `${_.round(this.last.pufferPercent).toString()}% \\-\\> `
    // }
    msg += `${_.round(eta.pufferPercent).toString()}%</u>\n`
    if (!_.isEmpty(eta.statusExtra)) {
      msg += `${eta.statusExtra}\n`
    }

    if (eta.pelletOn) {
      msg += `PelletAutomatik: ${eta.pelletStatus}${eta.pelletStatusExtra && eta.pelletStatusExtra != eta.statusExtra ? ` (${eta.pelletStatusExtra})` : ''}\n`
    }

    if (eta.pufferPercent <= WARN_THRESHOLD && statusMeaning !== 'heating') {
      msg += `Heiztemp ${eta.hausVorlauf}°C, Puffer ${eta.pufferTemp}°C\n`
    }

    return msg
  }

  makeKeyboard (eta: ETA | null, statusMeaning: string) {
    const buttons: any[] = []

    if (eta !== null) {
      // Sign-up
      if (this.data.signedUp === null) {
        if (this.warned || (eta.pufferPercent <= SIGNUP_THRESHOLD && statusMeaning === 'idle')) {
          buttons.push([this.bot.inlineButton(`${emoji.get('raising_hand')} Ich mach's`, { callback: 'signup' })])
        }
      }
      // // Pellet
      // if (eta.pelletOn || statusMeaning !== 'heating') {
      //   buttons.push([this.bot.inlineButton('PelletAutomatik ' + (eta.pelletOn ? 'ausschalten' : 'einschalten'), { callback: 'pellet-' + (eta.pelletOn ? 'off' : 'on') })])
      // }
    }

    this.msgHasKeyboard = buttons.length > 0
    if (this.msgHasKeyboard) {
      return this.bot.inlineKeyboard(buttons)
    } else {
      return undefined
    }
  }

  async handleSignup (event: TGCallbackQuery) {
    logger.info('Sign-Up from %o', event.from)
    this.data.signedUp = event.from
    if (this.last != null) {
      await this.update(this.last)
    }

    return await this.bot.answerCallbackQuery(event.id, { text: 'Du hast dich gemeldet' })
  }

  async handlePellet (event: TGCallbackQuery) {
    logger.info('Pellet command from %o: %o', event.from, event.data)
    if (!['pellet-on', 'pellet-off'].includes(event.data)) {
      logger.error('Invalid pellet data: %o', event.data)
      return await this.bot.answerCallbackQuery(event.id, { text: 'Error: invalid data: ' + event.data })
    }
    const newState = event.data === 'pellet-on'
    await api.setPellet(newState)

    return await this.bot.answerCallbackQuery(event.id, { text: (newState ? 'Aktivierung' : 'Deaktivierung') + ' beauftragt - bitte kurz warten' })
  }
}
