import TeleBot from 'telebot';


interface TeleBotExtension {
    adminMessage: (text: string, opt?: {
        parseMode?: string | undefined;
        replyToMessage?: number | undefined;
        replyMarkup?: any;
        notification?: boolean | undefined;
        webPreview?: boolean | undefined;
    } | undefined) => ReturnType<TeleBot['sendMessage']>

    safeEditMessage: (config: {
        chatId: string | number;
        messageId: number;
        inlineMsgId?: number;
    } | {
        chatId?: string | number;
        messageId?: number;
        inlineMsgId: number;
    }, text: string, options: object) => Promise<any> //TeleBot['editMessageText']

    addCmd(
        regex: RegExp,
        callback: (msg: TGMessage, match: string[]) => ReturnType<TeleBot.genericCb>
    ): void

}

type TeleBotExtended = TeleBot & TeleBotExtension