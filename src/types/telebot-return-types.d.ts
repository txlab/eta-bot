declare type TGUser = {
    id: number,
    is_bot: boolean,
    first_name: string,
    username: string,
    // language_code: string,
}

declare type TGMessage = {
    message_id: number,
    from: TGUser,
    text: string,
    
    reply: { 
        text: (text: string) => unknown
    }, 
}

declare type TGCallbackQuery = {
    id: string,
    from: TGUser,
    message: TGMessage,
    data: string,
}