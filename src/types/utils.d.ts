/* 
    make some properties optional.
    for nullable types, I only found this to work:
      `CopyWithPartial<NonNullable<Type>, 'field'>|null`
    
    source: https://www.designcise.com/web/tutorial/how-to-pick-some-properties-of-a-typescript-type-and-make-them-optional#include-all-properties-and-make-some-optional 
*/
export type CopyWithPartial<T extends Record<string, any>|null, K extends keyof T> = Omit<T, K> & Partial<T>;