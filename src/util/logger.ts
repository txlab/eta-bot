import _ from 'lodash'
import pino from 'pino'
import { ENVIRONMENT } from './secrets.js'

const isProd = ENVIRONMENT === 'production'

const logger = pino(_.merge(
  // config for all environments
  {
    translateTime: true,
    formatters: {
      // level labels https://github.com/pinojs/pino/issues/848#issuecomment-642768553
      level: (label: string) => ({ level: label })
    }
  },
  // env-specific config
  isProd ? { // prod config
    level: 'debug'
  } : { // non-prod config
    level: 'debug',
    transport: {
      target: 'pino-pretty',
      options: {
        ignore: 'pid,hostname,time' // TODO: short time instead of no time?
      }
    }
  }
))

if (!isProd) {
  logger.info('Logging initialized (prod=%s, env=%s)', isProd, ENVIRONMENT)
}
export default logger
