import dotenv from 'dotenv'

dotenv.config()

export const ENVIRONMENT = process.env.NODE_ENV

const assertEnv = (name: string): string => {
  const value = process.env[name]
  if (value) return value
  else { throw new Error('Missing env: ' + name) }
}

export const TELEGRAM_TOKEN = assertEnv('TELEGRAM_TOKEN')
export const TELEGRAM_ADMIN_CHAT = assertEnv('TELEGRAM_ADMIN_CHAT')
export const TELEGRAM_CHAT = assertEnv('TELEGRAM_CHAT')
export const ETA_URL = assertEnv('ETA_URL')
