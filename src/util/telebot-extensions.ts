
import logger from './logger.js'
import TeleBot from 'telebot'
import { TeleBotExtended } from 'telebot-extension'
import { TELEGRAM_ADMIN_CHAT } from './secrets.js'

export function extendTeleBot (botOrig: TeleBot): TeleBotExtended {
  const bot = botOrig as TeleBotExtended
  bot.adminMessage = async function (this: TeleBotExtended, ...args) {
    logger.info('Sending admin message: "%s"', args[0])
    return await this.sendMessage(TELEGRAM_ADMIN_CHAT, ...args)
  }
  bot.safeEditMessage = async function (this: TeleBotExtended, ...args) {
    try {
      return await this.editMessageText(...args)
    } catch (e) {
      if (e.description?.includes('message is not modified')) return false
      throw e
    }
  }
  bot.addCmd = (regex: RegExp, callback: (msg: TGMessage, match: string[]) => ReturnType<TeleBot.genericCb>) => {
    bot.on(regex, (msg: TGMessage, props: { match: string[] }) => {
      logger.info('Received command: %o', msg)
      return callback(msg, props.match)
    })
  }

  return bot
}
